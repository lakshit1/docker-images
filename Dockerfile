FROM ubuntu:latest

ENV UID=1000
ENV GID=1000
ENV USER="developer"
ENV TZ=Europe/Kiev
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt update && \
    apt install -y software-properties-common && \
    add-apt-repository ppa:git-core/ppa && \
    apt-get update && \
    apt-get install -y git make sudo

# create user
RUN groupadd --gid $GID $USER \
  && useradd -s /bin/bash --uid $UID --gid $GID -m $USER \
  && echo "ALL            ALL = (ALL) NOPASSWD: ALL" > /etc/sudoers

USER $USER
WORKDIR /home/$USER

RUN git clone https://gitlab.com/gitlab-org/gitlab-development-kit.git  && \
    cd gitlab-development-kit && \
    make bootstrap